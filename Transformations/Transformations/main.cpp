#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Shader.h"
#include <iostream>
using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

int main()
{
	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
#ifdef __APPLE__ //MAC OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。

	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	//加载并编译顶点着色器和片段着色器
	Shader ourShader("VertexShader.glsl", "FragmentShader.glsl");


	float vertices[] = {
		// 位置             //纹理坐标
		 0.5f,  0.5f, 0.0f, 1.0f,1.0f,//上 右
		 0.5f, -0.5f, 0.0f, 1.0f,0.0f,//下 右
		-0.5f, -0.5f, 0.0f, 0.0f,0.0f,//下 左
		-0.5f,  0.5f, 0.0f, 0.0f,1.0f //上 左
	};

	unsigned int indices[] = {
		0, 1, 3, // 第一个三角形
		1, 2, 3  // 第二个三角形
	};

	unsigned int VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	//绑定VAO
	glBindVertexArray(VAO);
	//绑定VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//绑定EBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);


	// 设置顶点位置属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	
	//设置纹理属性
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);


	//创建纹理对象
	unsigned int texture1, texture2;

	//纹理1
	glGenTextures(1, &texture1);//第一个参数：生成纹理的数量 第二个参数：纹理的ID引用
	glBindTexture(GL_TEXTURE_2D, texture1);//所有接下来的GL_TEXTURE_2D操作都作用于这个纹理对象

	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//设置纹理过滤参数
	//当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//加载图片
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);
	if (data)
	{
		/*
		第一个参数指定了纹理目标(Target)。
		设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理
		（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。

		第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。
		这里我们填0，也就是基本级别。

		第三个参数告诉OpenGL我们希望把纹理储存为何种格式。
		我们的图像只有RGB值，因此我们也把纹理储存为RGB值。

		第四个和第五个参数设置最终的纹理的宽度和高度。
		我们之前加载图像的时候储存了它们，所以我们使用对应的变量。


		第六个参数应该总是被设为0（历史遗留的问题）。


		第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，
		并把它们储存为char(byte)数组，我们将会传入对应值。

		最后一个参数是真正的图像数据。

		*/
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "加载纹理失败" << std::endl;
	}
	stbi_image_free(data);

	//纹理2
	glGenTextures(1, &texture2);//第一个参数：生成纹理的数量 第二个参数：纹理的ID引用
	glBindTexture(GL_TEXTURE_2D, texture2);//所有接下来的GL_TEXTURE_2D操作都作用于这个纹理对象

	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//设置纹理过滤参数
	//当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	data = stbi_load("awesomeface.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		std::cout << "加载纹理失败" << std::endl;
	}
	stbi_image_free(data);

	//告诉opengl每个采样器属于哪个采样单元  在进入while之前，只需要设置一次即可
	ourShader.use();
	glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);//直接用glUniform1i进行设置
	ourShader.setInt("texture2", 1);//也可以使用封装后的


	while (!glfwWindowShouldClose(window))
	{
		//输入
		processInput(window);

		//渲染底色
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//绑定纹理到对应的纹理单元
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);

		//初始化单位矩阵——0.9.9及以上版本必须显示初始化
		//因为默认会将矩阵类型初始化为一个零矩阵（所有元素均为0）
		glm::mat4 transform = glm::mat4(1.0);
		transform = glm::translate(transform,glm::vec3(0.5f, -0.5f, 0.0f));//将物体位移到相应位置
		//用glm::radians将角度转化为弧度，glm::vec3(0.0, 0.0, 1.0)表示沿Z轴旋转
		//transform = glm::rotate(transform,glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	    transform = glm::rotate(transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));//按照每一次时间的角度旋转

		//练习1：先旋转后位移
		/*transform = glm::rotate(transform, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));
		transform = glm::translate(transform, glm::vec3(0.5f, -0.5f, 0.0f));*/

		//获取矩阵的uniform位置并且设置矩阵
		ourShader.use();
		unsigned int transformLoc = glGetUniformLocation(ourShader.ID, "transform");
		glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(transform));


		// 使用着色器程序 绘制三角形
		ourShader.use();
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		//练习2
		transform = glm::mat4(1.0f); //重新设置单位矩阵
		transform = glm::translate(transform, glm::vec3(-0.5f, 0.5f, 0.0f));//位移到左上角
		float scaleAmount = sin(glfwGetTime());
		transform = glm::scale(transform, glm::vec3(scaleAmount, scaleAmount, scaleAmount));//用glm::vec3乘以该矩阵对其进行缩放
		glUniformMatrix4fv(transformLoc, 1, GL_FALSE, &transform[0][0]);//glm::value_ptr(transform)本质上就是取第一个元素地址，也就是矩阵首地址

		//使用新的变换再次绘制
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);

	glfwTerminate();
	return 0;
}

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}