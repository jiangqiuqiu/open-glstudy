#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
#include "Shader.h"
#include <iostream>
using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

int main()
{
	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
#ifdef __APPLE__ //MAC OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。

	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	glEnable(GL_DEPTH_TEST);//启用深度测试

	//加载并编译顶点着色器和片段着色器
	Shader ourShader("VertexShader.glsl", "FragmentShader.glsl");


	float vertices[] = {
		//顶点位置        //纹理坐标
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
	 0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	 0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
	-0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
	-0.5f,  0.5f, -0.5f,  0.0f, 1.0f
	};

	glm::vec3 cubePositions[] = {
	  glm::vec3(0.0f,  0.0f,  0.0f),
	  glm::vec3(2.0f,  5.0f, -15.0f),
	  glm::vec3(-1.5f, -2.2f, -2.5f),
	  glm::vec3(-3.8f, -2.0f, -12.3f),
	  glm::vec3(2.4f, -0.4f, -3.5f),
	  glm::vec3(-1.7f,  3.0f, -7.5f),
	  glm::vec3(1.3f, -2.0f, -2.5f),
	  glm::vec3(1.5f,  2.0f, -2.5f),
	  glm::vec3(1.5f,  0.2f, -1.5f),
	  glm::vec3(-1.3f,  1.0f, -1.5f)
	};

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	//绑定VAO
	glBindVertexArray(VAO);
	//绑定VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	// 设置顶点位置属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);

	//设置纹理属性
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);


	//创建纹理对象
	unsigned int texture1, texture2;

	//纹理1
	glGenTextures(1, &texture1);//第一个参数：生成纹理的数量 第二个参数：纹理的ID引用
	glBindTexture(GL_TEXTURE_2D, texture1);//所有接下来的GL_TEXTURE_2D操作都作用于这个纹理对象

	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//设置纹理过滤参数
	//当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	//加载图片
	int width, height, nrChannels;
	stbi_set_flip_vertically_on_load(true);
	unsigned char *data = stbi_load("container.jpg", &width, &height, &nrChannels, 0);
	if (data)
	{
		/*
		第一个参数指定了纹理目标(Target)。
		设置为GL_TEXTURE_2D意味着会生成与当前绑定的纹理对象在同一个目标上的纹理
		（任何绑定到GL_TEXTURE_1D和GL_TEXTURE_3D的纹理不会受到影响）。

		第二个参数为纹理指定多级渐远纹理的级别，如果你希望单独手动设置每个多级渐远纹理的级别的话。
		这里我们填0，也就是基本级别。

		第三个参数告诉OpenGL我们希望把纹理储存为何种格式。
		我们的图像只有RGB值，因此我们也把纹理储存为RGB值。

		第四个和第五个参数设置最终的纹理的宽度和高度。
		我们之前加载图像的时候储存了它们，所以我们使用对应的变量。


		第六个参数应该总是被设为0（历史遗留的问题）。


		第七第八个参数定义了源图的格式和数据类型。我们使用RGB值加载这个图像，
		并把它们储存为char(byte)数组，我们将会传入对应值。

		最后一个参数是真正的图像数据。

		*/
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "加载纹理失败" << std::endl;
	}
	stbi_image_free(data);

	//纹理2
	glGenTextures(1, &texture2);//第一个参数：生成纹理的数量 第二个参数：纹理的ID引用
	glBindTexture(GL_TEXTURE_2D, texture2);//所有接下来的GL_TEXTURE_2D操作都作用于这个纹理对象

	//设置纹理环绕方式
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//设置纹理过滤参数
	//当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	data = stbi_load("awesomeface.png", &width, &height, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

	}
	else
	{
		std::cout << "加载纹理失败" << std::endl;
	}
	stbi_image_free(data);

	//告诉opengl每个采样器属于哪个采样单元  在进入while之前，只需要设置一次即可
	ourShader.use();
	glUniform1i(glGetUniformLocation(ourShader.ID, "texture1"), 0);//直接用glUniform1i进行设置
	ourShader.setInt("texture2", 1);//也可以使用封装后的


	while (!glfwWindowShouldClose(window))
	{
		//输入
		processInput(window);

		//渲染底色
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);

		//因为使用了深度测试，我们也想要在每次渲染迭代之前清除深度缓冲（否则前一帧的深度信息仍然保存在缓冲中）。
		//就像清除颜色缓冲一样，我们可以通过在glClear函数中指定DEPTH_BUFFER_BIT位来清除深度缓冲
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//绑定纹理到对应的纹理单元
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture1);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);


		ourShader.use();
		//
		//创建变换矩阵 局部空间-》世界空间-》观察空间-》裁剪空间
		//
		// 确保首先初始化单位矩阵
		glm::mat4 view = glm::mat4(1.0f);//世界空间-》观察空间
		glm::mat4 projection = glm::mat4(1.0f);//观察空间-》裁剪空间

		view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));//将矩阵向我们要进行移动场景的反方向移动，也就是-z方向
		//做透视投影 
		projection = glm::perspective(glm::radians(45.0f), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
		/*
			   第一个参数定义了fov的值，它表示的是视野(Field of View)，并且设置了观察空间的大小。
			   如果想要一个真实的观察效果，它的值通常设置为45.0f，
			   但想要一个末日风格的结果你可以将其设置一个更大的值。
			   第二个参数设置了宽高比，由视口的宽除以高所得。
			   第三和第四个参数设置了平截头体的近和远平面。
			   我们通常设置近距离为0.1f，而远距离设为100.0f。
			   所有在近平面和远平面内且处于平截头体内的顶点都会被渲染。
		*/

		ourShader.setMat4("view", view);
		ourShader.setMat4("projection", projection);

		//使用新的变换再次绘制
		glBindVertexArray(VAO);

		//即将渲染十次
		for (int i = 0; i < 10; i++)
		{
			glm::mat4 model = glm::mat4(1.0f); //局部空间-》世界空间
			model = glm::translate(model,cubePositions[i]);//先平移
			float angle = 20.0f * i;
			model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));//再旋转
			ourShader.setMat4("model", model);//设置uniform model
			glDrawArrays(GL_TRIANGLES, 0, 36);//绘制立方体
		}

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);


	glfwTerminate();
	return 0;
}

void processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}