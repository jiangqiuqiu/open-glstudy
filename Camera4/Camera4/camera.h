#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>

//定义几个相机移动的可能选项
enum Camera_Movement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT
};

//摄像机参数值
const float SENSITIVITY = 0.1f;//灵敏度
const float ZOOM = 45.0f;//视野角
const float YAW = -90.0f;//偏航角
const float PITCH = 0.0f;//俯仰角
const float SPEED = 2.5f;//速度

class Camera
{
public:
	//摄像机属性
	glm::vec3 Position;//摄像机位置
	glm::vec3 Front;//方向向量
	glm::vec3 Up;//上向量
	glm::vec3 Right;//右向量 x轴
	glm::vec3 WorldUp;//z轴

	//欧拉角
	float Yaw;//偏航角
	float Pitch;//俯仰角

	//摄像机选项
	float MovementSpeed;//移动速度
	float MouseSensitivity;//精准度
	float Zoom;//视野角度

	Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), //物体位置默认为0,0,0
		   glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), 
		   float yaw = YAW, 
		   float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)),
		                          MovementSpeed(SPEED),
		                          MouseSensitivity(SENSITIVITY), 
		                          Zoom(ZOOM)
	{
		Position = position;
		WorldUp = up;
		Yaw = yaw;
		Pitch = pitch;
		updateCameraVectors();
	}

	Camera(float posX, 
		   float posY, 
		   float posZ, 
		   float upX, 
		   float upY, 
		   float upZ, 
		   float yaw, 
		   float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), 
		                  MovementSpeed(SPEED), 
		                  MouseSensitivity(SENSITIVITY), 
		                  Zoom(ZOOM)
	{
		Position = glm::vec3(posX, posY, posZ);
		WorldUp = glm::vec3(upX, upY, upZ);
		Yaw = yaw;
		Pitch = pitch;
		updateCameraVectors();
	}

	//使用欧拉角和观察函数获得观察矩阵
	glm::mat4 GetViewMatrix()
	{
		return glm::lookAt(Position, Position + Front, Up);
	}

	//根据键盘的输入移动摄像机位置
	void ProcessKeyboard(Camera_Movement direction, float deltaTime)
	{
		float velocity = MovementSpeed * deltaTime;
		if (direction == FORWARD)
			Position += Front * velocity;
		if (direction == BACKWARD)
			Position -= Front * velocity;
		if (direction == LEFT)
			Position -= Right * velocity;
		if (direction == RIGHT)
			Position += Right * velocity;

		//练习1
		//让y轴为0，可以限制到xz平面
		Position.y = 0.0f;
	}

	void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true)
	{
		xoffset *= MouseSensitivity;
		yoffset *= MouseSensitivity;

		Yaw += xoffset;
		Pitch += yoffset;

		
		if (constrainPitch)
		{
			if (Pitch > 89.0f)
				Pitch = 89.0f;
			if (Pitch < -89.0f)
				Pitch = -89.0f;
		}

		updateCameraVectors();
	}

	void ProcessMouseScroll(float yoffset)
	{
		Zoom -= (float)yoffset;
		if (Zoom < 1.0f)
			Zoom = 1.0f;
		if (Zoom > 45.0f)
			Zoom = 45.0f;
	}

private:
	void updateCameraVectors()
	{
		//更新方向向量
		glm::vec3 front;
		front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		front.y = sin(glm::radians(Pitch));
		front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
		Front = glm::normalize(front);
		// 重新计算右向量和上向量
		Right = glm::normalize(glm::cross(Front, WorldUp));  
		Up = glm::normalize(glm::cross(Right, Front));
	}
};


#endif // !CAMERA_H
