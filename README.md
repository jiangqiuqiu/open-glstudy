# OpenGLStudy

#### 介绍
用于记录OpenGL的学习   
参考网站为LearnOpenGL CN https://learnopengl-cn.github.io/  
现代计算机图形学-闫令琪 https://www.bilibili.com/video/BV1X7411F744

#### 环境和配置

1.  VS2017
2.  OpenGL3.3版本


#### 目录

1.  FirstOpenGL                 第一个OpenGL程序  
2.  HelloTriangle               绘制三角形—VAO,VBO、利用EBO绘制三角形  
3.  HelloTriangleExercise1      三角形练习1
4.  HelloTriangleExercise2      你好三角形练习2  
5.  HelloTriangleExercise3      你好三角形练习3  
6.  GLSLDemo                    GLSL学习  
7.  ShaderUtil                  着色器工具类  
8.  ShaderExcersice             着色器练习  
9.  TexturesDemo                纹理1    
10. TexturesDemo2               纹理2、纹理练习1  
11. TextureExercise2            纹理练习2  
12. TextureExercise3            纹理练习3  
13. TextureExercise4            纹理练习4  
14. Transformations             变换、练习1、练习2 
15. CoordinateSystems1          坐标1-3D  
16. CoordinateSystems2          坐标2 更多3D  
17. CoordinateSystems3          坐标3 更多立方体  
18. CoordinateSystemExercise    坐标练习  
19. Camera1                     摄像机1  摄像机练习2  
20. Camera2                     摄像机2  
21. Camera3                     摄像机3  
22. Camera4                     摄像机4  摄像机练习1  
23. Color                       颜色




