#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

//定义顶点着色器
const char *vertexShaderSource =
"#version 330 core\n"//对应使用OpenGL3.3 核心版本
"layout (location = 0) in vec3 aPos;\n"//声明输入顶点属性  位置变量的属性位置值为 0 
"layout (location = 1) in vec3 aColor;\n"//颜色变量的属性位置值为 1
"out vec3 vertexColor;\n"//声明输出变量
"void main()\n"
"{"
"gl_Position=vec4(aPos.xyz,1.0);\n"//gl_Position  内置变量
"//vertexColor=vec4(0.5,0.0,0.0,1.0);\n"//把输出变量设置为暗红色
"vertexColor = aColor;\n"
"}";

//定义片段着色器
const char *framegmentShaderSource =
"#version 330 core\n"
"out vec4 FragColor;\n"//输出变量 最终输出的颜色
"in vec3 vertexColor;\n"//从顶点着色器传来的输入变量（名称相同、类型相同）
"uniform vec4 ourColor;\n"//需要在OPENGL程序代码中设定这个变量
"void main()\n"
"{"
"//FragColor=vec4(1.0f,0.5f,0.2f,1.0f);\n"
"//FragColor=vertexColor;\n"
"//FragColor=ourColor;\n"
"FragColor=vec4(vertexColor,1.0);"
"}";


int main()
{

	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
#ifdef __APPLE__ //MAC OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。

	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	//
	//分别创建顶点着色器和片段着色器
	//

	//1、创建顶点着色器对象
	unsigned int vertexShader;
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	//把这个着色器源码附加到着色器对象上，然后编译它
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);//编译着色器

	//检测编译是否成功
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		cout << "顶点着色器编译失败" << infoLog << endl;
	}

	//2、创建片段着色器
	unsigned int fragmentShader;
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	//把片元着色器源码附加到着色器对象上，然后编译它
	glShaderSource(fragmentShader, 1, &framegmentShaderSource, NULL);
	glCompileShader(fragmentShader);//编译着色器

	//检测编译是否成功		
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		cout << "片元着色器编译失败" << infoLog << endl;
	}

	//3、将两个着色器链接到着色器程序
	unsigned int shaderProgram;
	shaderProgram = glCreateProgram();
	//把之前编译的着色器附加到程序对象上，然后用glLinkProgram链接它们
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
		cout << "着色器程序对象链接失败！" << infoLog << endl;
	}

	//在把着色器对象链接到程序对象以后，记得删除着色器对象
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

	
	//float vertices[] = {
	//	0.5f,  0.5f, 0.0f,  // 右上角
	//	0.5f, -0.5f, 0.0f,  // 右下角
	//   -0.5f, -0.5f, 0.0f,  // 左下角
	//   -0.5f,  0.5f, 0.0f   // 左上角
	//};

	float vertices[] = {
		// 位置              // 颜色
		 0.5f, -0.5f, 0.0f,  1.0f, 0.0f, 0.0f,   // 右下
		-0.5f, -0.5f, 0.0f,  0.0f, 1.0f, 0.0f,   // 左下
		 0.0f,  0.5f, 0.0f,  0.0f, 0.0f, 1.0f    // 顶部
	};

	unsigned int indices[] = {  //注意索引要从0开始
		0, 1, 3,  // 第一个三角形
		1, 2, 3   // 第二个三角形
	};

	unsigned int VBO, VAO, EBO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);//生成索引缓冲对象

	//绑定VAO
	glBindVertexArray(VAO);

	//绑定VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	//传递数据到VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	//绑定EBO
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	//传递数据到EBO
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	//设置顶点的位置属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);//启用该顶点属性

	// 设置顶点的颜色属性
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);//启用该顶点属性

	//可以解绑定VBO和VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	/*
	线框模式(Wireframe Mode)

	要想用线框模式绘制你的三角形，你可以通过glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)函数配置
	OpenGL如何绘制图元。第一个参数表示我们打算将其应用到所有的三角形的正面和背面，
	第二个参数告诉我们用线来绘制。之后的绘制调用会一直以线框模式绘制三角形，
	直到我们用glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)将其设置回默认模式。
	*/
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	int nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	cout << "本机支持的最大顶点属性数量为: " << nrAttributes << endl;


	while (!glfwWindowShouldClose(window))
	{
		//输入 检测是否需要退出窗口
		processInput(window);

		//6、渲染指令
		//glClearColor来设置清空屏幕所用的颜色
		glClearColor(0.2f, 0.3f, 0.4f, 1.0f);//设置状态函数
		//glClear函数来清空屏幕的颜色缓冲，它接受一个缓冲位(Buffer Bit)来指定要清空的缓冲，
		//可能的缓冲位有GL_COLOR_BUFFER_BIT，GL_DEPTH_BUFFER_BIT和GL_STENCIL_BUFFER_BIT
		glClear(GL_COLOR_BUFFER_BIT);//状态使用函数 

		//使用着色器程序
		glUseProgram(shaderProgram);//激活着色器

		////更新uniform颜色—ourColor
		//float timeValue = glfwGetTime();//获取运行的秒数
		//float greenValue = sin(timeValue) / 2.0f + 0.5f;
		//float redValue = cos(timeValue) / 2.0f + 0.5f;
		////glGetUniformLocation查询uniform ourColor的位置值
		////我们为查询函数提供着色器程序和uniform的名字（这是我们希望获得的位置值的来源）
		//int vertexColorLocation = glGetUniformLocation(shaderProgram,"ourColor");
		////通过glUniform4f函数设置uniform值
		//glUniform4f(vertexColorLocation, redValue,greenValue,0.0f,0.0f);
		/*
		   注意，查询uniform地址不要求你之前使用过着色器程序，
		   但是更新一个uniform之前你必须先使用程序（调用glUseProgram)，
		   因为它是在当前激活的着色器程序中设置uniform的。


		    因为OpenGL在其核心是一个C库，所以它不支持类型重载，
			在函数参数不同的时候就要为其定义新的函数；glUniform是一个典型例子。
			这个函数有一个特定的后缀，标识设定的uniform的类型。可能的后缀有：

			后缀	含义
			f	函数需要一个float作为它的值
			i	函数需要一个int作为它的值
			ui	函数需要一个unsigned int作为它的值
			3f	函数需要3个float作为它的值
			fv	函数需要一个float向量/数组作为它的值
			每当你打算配置一个OpenGL的选项时就可以简单地根据这些规则选择适合你的数据类型的重载函数。
			在我们的例子里，我们希望分别设定uniform的4个float值，
			所以我们通过glUniform4f传递我们的数据(注意，我们也可以使用fv版本)。
		*/

		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);//使用VAO画三角形
		//glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);//使用EBO绘制
		//glDrawElements(GL_LINE_LOOP, 6, GL_UNSIGNED_INT, 0);
		/*
		  第一个参数指定了我们绘制的模式，这个和glDrawArrays的一样。
		  第二个参数是我们打算绘制顶点的个数，这里填6，也就是说我们一共需要绘制6个顶点。
		  第三个参数是索引的类型，这里是GL_UNSIGNED_INT。
		  最后一个参数里我们可以指定EBO中的偏移量
		  （或者传递一个索引数组，但是这是当你不在使用索引缓冲对象的时候），
		  但是我们会在这里填写0。
		*/

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
	glDeleteProgram(shaderProgram);

	glfwTerminate();
	system("pause");
	return 0;
}

//定义这个回调函数
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window)
{
	/*
	 检查用户是否按下了返回键(Esc)
	（如果没有按下，glfwGetKey将会返回GLFW_RELEASE。如果用户的确按下了返回键，
	我们将通过glfwSetwindowShouldClose使用把WindowShouldClose属性设置为 true的方法关闭GLFW。
	*/

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
	else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

}