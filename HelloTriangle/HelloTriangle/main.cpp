#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;

/*
	顶点数组对象：Vertex Array Object，VAO
	顶点缓冲对象：Vertex Buffer Object，VBO
	索引缓冲对象：Element Buffer Object，EBO或Index Buffer Object，IBO

	本节重点知识：
	1、图形渲染管线
	图形渲染管线接受一组3D坐标，然后把它们转变为你屏幕上的有色2D像素输出。
	图形渲染管线可以被划分为几个阶段，每个阶段将会把前一个阶段的输出作为输入。
	所有这些阶段都是高度专门化的（它们都有一个特定的函数），并且很容易并行执行。
	正是由于它们具有并行执行的特性，当今大多数显卡都有成千上万的小处理核心，
	它们在GPU上为每一个（渲染管线）阶段运行各自的小程序，从而在图形渲染管线中快速处理你的数据。
	这些小程序叫做着色器(Shader)。
	有些着色器允许开发者自己配置，这就允许我们用自己写的着色器来替换默认的。
	这样我们就可以更细致地控制图形渲染管线中的特定部分了，
	而且因为它们运行在GPU上，所以它们可以给我们节约宝贵的CPU时间。
	OpenGL着色器是用OpenGL着色器语言(OpenGL Shading Language, GLSL)写成的。

	图形渲染管线：输入顶点数据-》顶点着色器—》形状（图元）装配—》几何着色器—》光栅化—》片段着色器—》测试与混合
*/



//声明对窗口注册一个回调函数(Callback Function)，它会在每次窗口大小被调整的时候被调用
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
//声明processInput，用于处理接收输入的函数
void processInput(GLFWwindow *window);

//定义顶点着色器
const char *vertexShaderSource = 
			"#version 330 core\n"//对应使用OpenGL3.3 核心版本
			"layout (location = 0) in vec3 aPos;\n"//声明输入顶点属性
			"void main()\n"
			"{"
			"gl_Position=vec4(aPos.x,aPos.y,aPos.z,1.0);"//gl_Position  内置变量
			"}";

//定义片段着色器
const char *framegmentShaderSource = 
			"#version 330 core\n"
			"out vec4 FragColor;\n"//输出变量 最终输出的颜色
			"void main()\n"
			"{"
			"FragColor=vec4(1.0f,0.5f,0.2f,1.0f);"
			"}";

int main()
{
	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
#ifdef __APPLE__ //MAC OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。


	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	//三角形开始绘制
	//{
		/*
			1、顶点输入
			开始绘制图形之前，我们必须先给OpenGL输入一些顶点数据。
			OpenGL是一个3D图形库，所以我们在OpenGL中指定的所有坐标都是3D坐标（x、y和z）。
			OpenGL不是简单地把所有的3D坐标变换为屏幕上的2D像素；
			OpenGL仅当3D坐标在3个轴（x、y和z）上都为-1.0到1.0的范围内时才处理它。
			所有在所谓的标准化设备坐标(Normalized Device Coordinates)范围内的坐标才会最终呈现在屏幕上
			（在这个范围以外的坐标都不会显示）。

			由于OpenGL是在3D空间中工作的，而我们渲染的是一个2D三角形，
			我们将它顶点的z坐标设置为0.0。
			这样子的话三角形每一点的深度(Depth)都是一样的，从而使它看上去像是2D的。

			通常深度可以理解为z坐标，它代表一个像素在空间中和你的距离，
			如果离你远就可能被别的像素遮挡，你就看不到它了，它会被丢弃，以节省资源。

			标准化设备坐标(Normalized Device Coordinates, NDC)
			一旦你的顶点坐标已经在顶点着色器中处理过，它们就应该是标准化设备坐标了，
			标准化设备坐标是一个x、y和z值在-1.0到1.0的一小段空间。
			任何落在范围外的坐标都会被丢弃/裁剪，不会显示在你的屏幕上。
			与通常的屏幕坐标不同，y轴正方向为向上，(0, 0)坐标是这个图像的中心，而不是左上角。
			最终你希望所有(变换过的)坐标都在这个坐标空间中，否则它们就不可见了。
			你的标准化设备坐标接着会变换为屏幕空间坐标(Screen-space Coordinates)，
			这是使用你通过glViewport函数提供的数据，进行视口变换(Viewport Transform)完成的。
			所得的屏幕空间坐标又会被变换为片段输入到片段着色器中。
		*/
		float vertices[] = {
		 0.5f,  0.5f, 0.0f,  // top right
		 0.5f, -0.5f, 0.0f,  // bottom right
		-0.5f, -0.5f, 0.0f,  // bottom left
		-0.5f,  0.5f, 0.0f   // top left 
		};
		
		
		/*
			定义这样的顶点数据以后，我们会把它作为输入发送给图形渲染管线的第一个处理阶段：顶点着色器。
			它会在GPU上创建内存用于储存我们的顶点数据，还要配置OpenGL如何解释这些内存，并且指定其如何发送给显卡。
			顶点着色器接着会处理我们在内存中指定数量的顶点。

			我们通过顶点缓冲对象(Vertex Buffer Objects, VBO)管理这个内存，
			它会在GPU内存（通常被称为显存）中储存大量顶点。
			使用这些缓冲对象的好处是我们可以一次性的发送一大批数据到显卡上，而不是每个顶点发送一次。
			从CPU把数据发送到显卡相对较慢，所以只要可能我们都要尝试尽量一次性发送尽可能多的数据。
			当数据发送至显卡的内存中后，顶点着色器几乎能立即访问顶点，这是个非常快的过程。

		*/
		
		unsigned int VBO;//定义缓冲ID
		glGenBuffers(1,&VBO);//△生成一个VBO对象，传入VBO的地址，将ID保存到这个变量中

		/*
			顶点数组对象
			顶点数组对象(Vertex Array Object, VAO)可以像顶点缓冲对象那样被绑定，
			任何随后的顶点属性调用都会储存在这个VAO中。
			这样的好处就是，当配置顶点属性指针时，你只需要将那些调用执行一次，
			之后再绘制物体的时候只需要绑定相应的VAO就行了。
			这使在不同顶点数据和属性配置之间切换变得非常简单，只需要绑定不同的VAO就行了。
			刚刚设置的所有状态都将存储在VAO中。

			OpenGL的核心模式要求我们使用VAO，所以它知道该如何处理我们的顶点输入。
			如果我们绑定VAO失败，OpenGL会拒绝绘制任何东西。

			一个顶点数组对象会储存以下这些内容：
			1、glEnableVertexAttribArray和glDisableVertexAttribArray的调用。
			2、通过glVertexAttribPointer设置的顶点属性配置。
			3、通过glVertexAttribPointer调用与顶点属性关联的顶点缓冲对象。
		*/
		//创建一个VAO对象
		unsigned int VAO;
		glGenVertexArrays(1, &VAO);

		/*
		   要想使用VAO，要做的只是使用glBindVertexArray绑定VAO。
		   从绑定之后起，我们应该绑定和配置对应的VBO和属性指针，之后解绑VAO供之后使用。
		   当我们打算绘制一个物体的时候，
		   我们只要在绘制物体前简单地把VAO绑定到希望使用的设定上就行了。
		*/
		//绑定VAO
		glBindVertexArray(VAO);


		//顶点缓冲对象的缓冲类型是GL_ARRAY_BUFFER
		//可以使用glBindBuffer函数把新创建的缓冲绑定到GL_ARRAY_BUFFER目标上
		glBindBuffer(GL_ARRAY_BUFFER,VBO);//△绑定缓冲到目标缓冲

		//调用glBufferData函数，它会把之前定义的顶点数据复制到缓冲的内存中
		glBufferData(GL_ARRAY_BUFFER,sizeof(vertices),vertices,GL_STATIC_DRAW);//△传递数据到缓冲
		/*
		   glBufferData是一个专门用来把用户定义的数据复制到当前绑定缓冲的函数。
		   它的第一个参数是目标缓冲的类型：顶点缓冲对象当前绑定到GL_ARRAY_BUFFER目标上。
		   第二个参数指定传输数据的大小(以字节为单位)；用一个简单的sizeof计算出顶点数据大小就行。
		   第三个参数是我们希望发送的实际数据。
		   第四个参数指定了我们希望显卡如何管理给定的数据。它有三种形式：
					GL_STATIC_DRAW ：数据不会或几乎不会改变。
					GL_DYNAMIC_DRAW：数据会被改变很多。
					GL_STREAM_DRAW ：数据每次绘制时都会改变。

		  三角形的位置数据不会改变，每次渲染调用时都保持原样，所以它的使用类型最好是GL_STATIC_DRAW。
		  如果，比如说一个缓冲中的数据将频繁被改变，那么使用的类型就是GL_DYNAMIC_DRAW或GL_STREAM_DRAW，
		  这样就能确保显卡把数据放在能够高速写入的内存部分。
		*/


		/*
			2、顶点着色器
			顶点着色器(Vertex Shader)是几个可编程着色器中的一个。
			如果我们打算做渲染的话，现代OpenGL需要我们至少设置一个顶点和一个片段着色器。
			见上方定义顶点着色器 vertexShaderSource
		*/

		/*
			3、编译着色器
			我们已经写了一个顶点着色器源码（储存在一个C的字符串中），但是为了能够让OpenGL使用它，
			我们必须在运行时动态编译它的源码。
		*/
		//创建着色器对象
		unsigned int vertexShader;
		//我们把需要创建的着色器类型以参数形式提供给glCreateShader。
		//由于我们正在创建一个顶点着色器，传递的参数是GL_VERTEX_SHADER
		vertexShader = glCreateShader(GL_VERTEX_SHADER);

		//把这个着色器源码附加到着色器对象上，然后编译它
		glShaderSource(vertexShader,1,&vertexShaderSource,NULL);
		glCompileShader(vertexShader);//编译着色器
		
		//检测编译是否成功
		int success;
		char infoLog[512];
		glGetShaderiv(vertexShader,GL_COMPILE_STATUS,&success);

		//如果编译失败打印错误信息
		if (!success)
		{
			glGetShaderInfoLog(vertexShader,512,NULL,infoLog);
			cout << "顶点着色器编译失败" <<infoLog<< endl;
		}

		/*
			4、片段着色器——也称片元着色器
			片段着色器所做的是计算像素最后的颜色输出。

			在计算机图形中颜色被表示为有4个元素的数组：红色、绿色、蓝色和alpha(透明度)分量，通常缩写为RGBA。
			当在OpenGL或GLSL中定义一个颜色的时候，我们把颜色每个分量的强度设置在0.0到1.0之间。
			比如说我们设置红为1.0f，绿为1.0f，我们会得到两个颜色的混合色，即黄色。
			这三种颜色分量的不同调配可以生成超过1600万种不同的颜色！
		*/

		//创建着色器对象
		unsigned int fragmentShader;
		fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		//把片元着色器源码附加到着色器对象上，然后编译它
		glShaderSource(fragmentShader,1,&framegmentShaderSource,NULL);
		glCompileShader(fragmentShader);//编译着色器

		//检测编译是否成功		
		glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);

		//如果编译失败打印错误信息
		if (!success)
		{
			glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
			cout << "片元着色器编译失败" << infoLog << endl;
		}

		//两个着色器现在都编译了，
		//剩下的事情是把两个着色器对象链接到一个用来渲染的着色器程序(Shader Program)中。

		/*
			着色器程序
			着色器程序对象(Shader Program Object)是多个着色器合并之后并最终链接完成的版本。
			如果要使用刚才编译的着色器我们必须把它们链接(Link)为一个着色器程序对象，
			然后在渲染对象的时候激活这个着色器程序。
			已激活着色器程序的着色器将在我们发送渲染调用的时候被使用。

			当链接着色器至一个程序的时候，它会把每个着色器的输出链接到下个着色器的输入。
			当输出和输入不匹配的时候，你会得到一个连接错误。
		*/
		//glCreateProgram函数创建一个程序，并返回新创建程序对象的ID引用
		unsigned int shaderProgram;
		shaderProgram = glCreateProgram();

		//把之前编译的着色器附加到程序对象上，然后用glLinkProgram链接它们
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram,fragmentShader);
		glLinkProgram(shaderProgram);

		glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
		if (!success)
		{
			glGetProgramInfoLog(shaderProgram,512,NULL,infoLog);
			cout << "着色器程序对象链接失败！" << infoLog << endl;
		}

		//使用着色器对象
		//得到的结果就是一个程序对象，我们可以调用glUseProgram函数，
		//用刚创建的程序对象作为它的参数，以激活这个程序对象
		//在glUseProgram函数调用之后，
		//每个着色器调用和渲染调用都会使用这个程序对象（也就是之前写的着色器)了
		//glUseProgram(shaderProgram);


		//在把着色器对象链接到程序对象以后，记得删除着色器对象
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);


		//现在，我们已经把输入顶点数据发送给了GPU，并指示了GPU如何在顶点和片段着色器中处理它。
		//就快要完成了，但还没结束，OpenGL还不知道它该如何解释内存中的顶点数据，
		//以及它该如何将顶点数据链接到顶点着色器的属性上。我们需要告诉OpenGL怎么做。

		/*
		   5、链接顶点属性
		   顶点着色器允许我们指定任何以顶点属性为形式的输入。
		   这使其具有很强的灵活性的同时，
		   它还的确意味着我们必须手动指定输入数据的哪一个部分对应顶点着色器的哪一个顶点属性。
		   所以，我们必须在渲染前指定OpenGL该如何解释顶点数据。
		*/
		glVertexAttribPointer(0,3,GL_FLOAT,GL_FALSE,3*sizeof(float),(void*)0);
		glEnableVertexAttribArray(0);
		/*
			glVertexAttribPointer参数说明：
			1、第一个参数指定我们要配置的顶点属性。
			   还记得我们在顶点着色器中使用layout(location = 0)定义了position顶点属性的位置值(Location)吗？
			   它可以把顶点属性的位置值设置为0。
			   因为我们希望把数据传递到这一个顶点属性中，所以这里我们传入0。

			   注：layout( location=index ) in vec4 position;
				   如果这个index和glVertexAttribPointer的第一个参数一样，
				   那么相关缓存区的数据就会传递到这个position变量中去

			2、第二个参数指定顶点属性的大小。顶点属性是一个vec3，它由3个值组成，所以大小是3。
			3、第三个参数指定数据的类型，这里是GL_FLOAT(GLSL中vec*都是由浮点数值组成的)。
			4、下个参数定义我们是否希望数据被标准化(Normalize)。如果我们设置为GL_TRUE，
			   所有数据都会被映射到0（对于有符号型signed数据是-1）到1之间。我们把它设置为GL_FALSE。
			5、第五个参数叫做步长(Stride)，它告诉我们在连续的顶点属性组之间的间隔。
			   由于下个组位置数据在3个float之后，我们把步长设置为3 * sizeof(float)。
			   要注意的是由于我们知道这个数组是紧密排列的（在两个顶点属性之间没有空隙）
			   我们也可以设置为0来让OpenGL决定具体步长是多少（只有当数值是紧密排列时才可用）。
			   一旦我们有更多的顶点属性，我们就必须更小心地定义每个顶点属性之间的间隔
			6、最后一个参数的类型是void*，所以需要我们进行这个奇怪的强制类型转换。
			   它表示位置数据在缓冲中起始位置的偏移量(Offset)。
			   由于位置数据在数组的开头，所以这里是0。
		*/


		/*以上的流程是：使用一个顶点缓冲对象将顶点数据初始化至缓冲中，
		  建立了一个顶点和一个片段着色器，
		  并告诉了OpenGL如何把顶点数据链接到顶点着色器的顶点属性上。
		*/
	//}


	while (!glfwWindowShouldClose(window))//glfwWindowShouldClose函数在我们每次循环的开始前检查一次GLFW是否被要求退出，如果是的话该函数返回true然后渲染循环便结束了，之后为我们就可以关闭应用程序了
	{
		//5、输入  检测是否需要退出窗口
		processInput(window);

		//6、渲染指令
		//glClearColor来设置清空屏幕所用的颜色
		glClearColor(0.2f, 0.3f, 0.4f, 1.0f);//设置状态函数
		//glClear函数来清空屏幕的颜色缓冲，它接受一个缓冲位(Buffer Bit)来指定要清空的缓冲，
		//可能的缓冲位有GL_COLOR_BUFFER_BIT，GL_DEPTH_BUFFER_BIT和GL_STENCIL_BUFFER_BIT
		glClear(GL_COLOR_BUFFER_BIT);//状态使用函数 

		//开始绘制三角形了
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		//参数1：图元类型
		//参数2：顶点的起始位置
		//参数3：绘制的顶点个数
		glDrawArrays(GL_LINE_LOOP, 0, 3);//VAO用这个
		//glDrawArrays(GL_TRIANGLES, 0, 3);//VAO用这个

		// 检查并调用事件，交换缓冲
		glfwSwapBuffers(window);//glfwSwapBuffers函数会交换颜色缓冲（它是一个储存着GLFW窗口每一个像素颜色值的大缓冲），它在这一迭代中被用来绘制，并且将会作为输出显示在屏幕上。
		glfwPollEvents();//glfwPollEvents函数检查有没有触发什么事件（比如键盘输入、鼠标移动等）、更新窗口状态，并调用对应的回调函数（可以通过回调方法手动设置）。
	}

	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteProgram(shaderProgram);

	//当渲染循环结束后我们需要正确释放/删除之前的分配的所有资源。
	//我们可以在main函数的最后调用glfwTerminate函数来完成。
	glfwTerminate();
	system("pause");
	return 0;
}

//定义这个回调函数
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

/*
		我们同样也希望能够在GLFW中实现一些输入控制，这可以通过使用GLFW的几个输入函数来完成。
		我们将会使用GLFW的glfwGetKey函数，它需要一个窗口以及一个按键作为输入。
		这个函数将会返回这个按键是否正在被按下。
		我们将创建一个processInput函数来让所有的输入代码保持整洁。
*/
void processInput(GLFWwindow *window)
{
	/*
	 检查用户是否按下了返回键(Esc)
	（如果没有按下，glfwGetKey将会返回GLFW_RELEASE。如果用户的确按下了返回键，
	我们将通过glfwSetwindowShouldClose使用把WindowShouldClose属性设置为 true的方法关闭GLFW。
	*/
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
}