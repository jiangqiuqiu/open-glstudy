#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;
uniform float xOffSet;
out vec4 ourPos;

void main()
{
  gl_Position=vec4(aPos.x+xOffSet,-aPos.y,aPos.z,1.0);//让三角形上下颠倒且让三角形沿着X轴移动
  ourColor=aColor;
  ourPos=vec4(aPos,1.0);//将顶点位置输出到片段着色器
}