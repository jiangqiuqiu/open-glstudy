#version 330 core
out vec4 FragColor;

in vec3 ourColor;
in vec4 ourPos;

void main()
{
    //FragColor = vec4(ourColor, 1.0f);
	FragColor=ourPos;
	//为什么左下角/左上角是黑色的？因为颜色的范围是(0.0,1.0)，该坐标是(-0.5,-0.5)，小于零的都按照0.0进行了设置，因此就是黑色的
}