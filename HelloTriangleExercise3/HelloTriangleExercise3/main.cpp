//创建两个着色器程序，第二个程序使用一个不同的片段着色器，输出黄色；
//再次绘制这两个三角形，让其中一个输出为黄色
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

//定义顶点着色器
const char *vertexShaderSource =
"#version 330 core\n"//对应使用OpenGL3.3 核心版本
"layout (location = 0) in vec3 aPos;\n"//声明输入顶点属性
"void main()\n"
"{"
"gl_Position=vec4(aPos.x,aPos.y,aPos.z,1.0);"//gl_Position  内置变量
"}";

//定义片段着色器1
const char *framegmentShaderSource1 =
"#version 330 core\n"
"out vec4 FragColor;\n"//输出变量 最终输出的颜色
"void main()\n"
"{"
"FragColor=vec4(1.0f,0.5f,0.2f,1.0f);"
"}";

//定义片段着色器2
const char *framegmentShaderSource2 =
"#version 330 core\n"
"out vec4 FragColor;\n"//输出变量 最终输出的颜色
"void main()\n"
"{"
"FragColor=vec4(1.0f, 1.0f, 0.0f, 1.0f);"
"}";


int main()
{

	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
#ifdef __APPLE__ //MAC OS
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (window == NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。

	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	//
	//分别创建顶点着色器和片段着色器
	//

	//1、创建顶点着色器对象1
	unsigned int vertexShader1;
	vertexShader1 = glCreateShader(GL_VERTEX_SHADER);
	//把这个着色器源码附加到着色器对象上，然后编译它
	glShaderSource(vertexShader1, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader1);//编译着色器

	//检测编译是否成功
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader1, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(vertexShader1, 512, NULL, infoLog);
		cout << "顶点着色器编译失败" << infoLog << endl;
	}

	//1、创建顶点着色器对象2
	unsigned int vertexShader2;
	vertexShader2 = glCreateShader(GL_VERTEX_SHADER);
	//把这个着色器源码附加到着色器对象上，然后编译它
	glShaderSource(vertexShader2, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader2);//编译着色器

	//检测编译是否成功
	success;
	infoLog[512];
	glGetShaderiv(vertexShader2, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(vertexShader2, 512, NULL, infoLog);
		cout << "顶点着色器编译失败" << infoLog << endl;
	}

	//2、创建片段着色器1
	unsigned int fragmentShader1;
	fragmentShader1 = glCreateShader(GL_FRAGMENT_SHADER);
	//把片元着色器源码附加到着色器对象上，然后编译它
	glShaderSource(fragmentShader1, 1, &framegmentShaderSource1, NULL);
	glCompileShader(fragmentShader1);//编译着色器

	//检测编译是否成功		
	glGetShaderiv(fragmentShader1, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader1, 512, NULL, infoLog);
		cout << "片元着色器编译失败" << infoLog << endl;
	}

	//2、创建片段着色器2
	unsigned int fragmentShader2;
	fragmentShader2 = glCreateShader(GL_FRAGMENT_SHADER);
	//把片元着色器源码附加到着色器对象上，然后编译它
	glShaderSource(fragmentShader2, 1, &framegmentShaderSource2, NULL);
	glCompileShader(fragmentShader2);//编译着色器

	//检测编译是否成功		
	glGetShaderiv(fragmentShader2, GL_COMPILE_STATUS, &success);

	//如果编译失败打印错误信息
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader2, 512, NULL, infoLog);
		cout << "片元着色器编译失败" << infoLog << endl;
	}

	//3、将两个着色器链接到着色器程序1
	unsigned int shaderProgram1;
	shaderProgram1 = glCreateProgram();
	//把之前编译的着色器附加到程序对象上，然后用glLinkProgram链接它们
	glAttachShader(shaderProgram1, vertexShader1);
	glAttachShader(shaderProgram1, fragmentShader1);
	glLinkProgram(shaderProgram1);
	glGetProgramiv(shaderProgram1, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram1, 512, NULL, infoLog);
		cout << "着色器程序对象链接失败！" << infoLog << endl;
	}

	//3、将两个着色器链接到着色器程序2
	unsigned int shaderProgram2;
	shaderProgram2 = glCreateProgram();
	//把之前编译的着色器附加到程序对象上，然后用glLinkProgram链接它们
	glAttachShader(shaderProgram2, vertexShader2);
	glAttachShader(shaderProgram2, fragmentShader2);
	glLinkProgram(shaderProgram2);
	glGetProgramiv(shaderProgram2, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(shaderProgram2, 512, NULL, infoLog);
		cout << "着色器程序对象链接失败！" << infoLog << endl;
	}

	//在把着色器对象链接到程序对象以后，记得删除着色器对象
	glDeleteShader(vertexShader1);
	glDeleteShader(fragmentShader1);

	glDeleteShader(vertexShader2);
	glDeleteShader(fragmentShader2);

	float firstTriangle[] = {
		-0.9f, -0.5f, 0.0f,  
		-0.0f, -0.5f, 0.0f,  
		-0.45f, 0.5f, 0.0f,  
	};
	float secondTriangle[] = {
		0.0f, -0.5f, 0.0f,  
		0.9f, -0.5f, 0.0f, 
		0.45f, 0.5f, 0.0f   
	};

	unsigned int VBO[2], VAO[2];
	glGenVertexArrays(2, VAO);
	glGenBuffers(2, VBO);
	
	//第一个三角形
	//绑定VAO
	glBindVertexArray(VAO[0]);
	//绑定VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
	//传递数据到缓冲
	glBufferData(GL_ARRAY_BUFFER, sizeof(firstTriangle), firstTriangle, GL_STATIC_DRAW);
	//设置0索引上的顶点属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);


	//第二个三角形
	//绑定VAO
	glBindVertexArray(VAO[1]);
	//绑定VBO
	glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
	//传递数据到缓冲
	glBufferData(GL_ARRAY_BUFFER, sizeof(secondTriangle), secondTriangle, GL_STATIC_DRAW);
	//设置0索引上的顶点属性
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);



	//可以解绑定VBO和VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	/*
	线框模式(Wireframe Mode)

	要想用线框模式绘制你的三角形，你可以通过glPolygonMode(GL_FRONT_AND_BACK, GL_LINE)函数配置
	OpenGL如何绘制图元。第一个参数表示我们打算将其应用到所有的三角形的正面和背面，
	第二个参数告诉我们用线来绘制。之后的绘制调用会一直以线框模式绘制三角形，
	直到我们用glPolygonMode(GL_FRONT_AND_BACK, GL_FILL)将其设置回默认模式。
	*/
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	while (!glfwWindowShouldClose(window))
	{
		//输入 检测是否需要退出窗口
		processInput(window);

		//6、渲染指令
		//glClearColor来设置清空屏幕所用的颜色
		glClearColor(0.2f, 0.3f, 0.4f, 1.0f);//设置状态函数
		//glClear函数来清空屏幕的颜色缓冲，它接受一个缓冲位(Buffer Bit)来指定要清空的缓冲，
		//可能的缓冲位有GL_COLOR_BUFFER_BIT，GL_DEPTH_BUFFER_BIT和GL_STENCIL_BUFFER_BIT
		glClear(GL_COLOR_BUFFER_BIT);//状态使用函数 

		//使用着色器程序1
		glUseProgram(shaderProgram1);
		glBindVertexArray(VAO[0]);
		glDrawArrays(GL_TRIANGLES, 0, 3);//使用VAO画三角形
		
		//使用着色器程序1
		glUseProgram(shaderProgram2);
		glBindVertexArray(VAO[1]);
		glDrawArrays(GL_TRIANGLES, 0, 3);//使用VAO画三角形

		/*
		  第一个参数指定了我们绘制的模式，这个和glDrawArrays的一样。
		  第二个参数是我们打算绘制顶点的个数，这里填6，也就是说我们一共需要绘制6个顶点。
		  第三个参数是索引的类型，这里是GL_UNSIGNED_INT。
		  最后一个参数里我们可以指定EBO中的偏移量
		  （或者传递一个索引数组，但是这是当你不在使用索引缓冲对象的时候），
		  但是我们会在这里填写0。
		*/

		// glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
		// -------------------------------------------------------------------------------
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glDeleteVertexArrays(2, VAO);
	glDeleteBuffers(2, VBO);
	glDeleteProgram(shaderProgram1);
	glDeleteProgram(shaderProgram2);

	glfwTerminate();
	system("pause");
	return 0;
}

//定义这个回调函数
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window)
{
	/*
	 检查用户是否按下了返回键(Esc)
	（如果没有按下，glfwGetKey将会返回GLFW_RELEASE。如果用户的确按下了返回键，
	我们将通过glfwSetwindowShouldClose使用把WindowShouldClose属性设置为 true的方法关闭GLFW。
	*/

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, true);
	}
	else if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

}