#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
using namespace std;

//声明对窗口注册一个回调函数(Callback Function)，它会在每次窗口大小被调整的时候被调用
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
//声明processInput，用于处理接收输入的函数
void processInput(GLFWwindow *window);

int main()
{
	glfwInit();//1、 初始化GLFW
	//使用glfwWindowHint配置GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);//主版本号是3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);//次版本号是3 说明:告诉GLFW我们要使用的OpenGL版本是3.3
	glfwWindowHint(GLFW_OPENGL_ANY_PROFILE,GLFW_OPENGL_CORE_PROFILE);//告诉GLFW我们使用的是核心模式(Core-profile)
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);//MAC OS需要放开这行

	//2、创建一个窗口对象
	GLFWwindow* window = glfwCreateWindow(800,600,"LearnOpenGL",NULL,NULL);
	if (window==NULL)
	{
		cout << "初始化GLFW窗口失败" << endl;
		glfwTerminate();
		return -1;
	}
	//创建完窗口后通知GLFW将窗口的上下文设置为当前线程的主上下文
	glfwMakeContextCurrent(window);

	//3、初始化GLAD——GLAD是用来管理OpenGL的函数指针的
	//给GLAD传入了用来加载系统相关的OpenGL函数指针地址的函数
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		cout << "初始化GLAD失败" << endl;
		return -1;
	}

	//4、视口
	//在我们开始渲染之前还有一件重要的事情要做，我们必须告诉OpenGL渲染窗口的尺寸大小，即视口(Viewport)，
	//这样OpenGL才只能知道怎样根据窗口大小显示数据和坐标。
	//我们可以通过调用glViewport函数来设置窗口的维度
	glViewport(0, 0, 800, 600);//glViewport函数前两个参数控制窗口左下角的位置。第三个和第四个参数控制渲染窗口的宽度和高度（像素）。

	/*注：实际上也可以将视口的维度设置为比GLFW的维度小，这样子之后所有的OpenGL渲染将会在一个更小的窗口中显示，
	这样子的话我们也可以将一些其它元素显示在OpenGL视口之外。
	
	OpenGL幕后使用glViewport中定义的位置和宽高进行2D坐标的转换，
	将OpenGL中的位置坐标转换为你的屏幕坐标。
	例如，OpenGL中的坐标(-0.5, 0.5)有可能（最终）被映射为屏幕中的坐标(200,450)。
	注意，处理过的OpenGL坐标范围只为-1到1，
	因此我们事实上将(-1到1)范围内的坐标映射到(0, 800)和(0, 600)。
	*/


	//当用户改变窗口的大小的时候，视口也应该被调整
	//注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数
	//回调函数是一个函数指针  该函数指针定义为:typedef void (* GLFWframebuffersizefun)(GLFWwindow*,int,int);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);


	/*我们可不希望只绘制一个图像之后我们的应用程序就立即退出并关闭窗口。
	我们希望程序在我们主动关闭它之前不断绘制图像并能够接受用户输入。
	因此，我们需要在程序中添加一个while循环，我们可以把它称之为渲染循环(Render Loop)，
	它能在我们让GLFW退出前一直保持运行*/
	while (!glfwWindowShouldClose(window))//glfwWindowShouldClose函数在我们每次循环的开始前检查一次GLFW是否被要求退出，如果是的话该函数返回true然后渲染循环便结束了，之后为我们就可以关闭应用程序了
	{
		/*
		双缓冲(Double Buffer)
		应用程序使用单缓冲绘图时可能会存在图像闪烁的问题。 
		这是因为生成的图像不是一下子被绘制出来的，而是按照从左到右，由上而下逐像素地绘制而成的。
		最终图像不是在瞬间显示给用户，而是通过一步一步生成的，这会导致渲染的结果很不真实。
		为了规避这些问题，我们应用双缓冲渲染窗口应用程序。
		前缓冲保存着最终输出的图像，它会在屏幕上显示；
		而所有的的渲染指令都会在后缓冲上绘制。
		当所有的渲染指令执行完毕后，我们交换(Swap)前缓冲和后缓冲，这样图像就立即呈显出来，
		之前提到的不真实感就消除了。
		*/
		//5、输入  检测是否需要退出窗口
		processInput(window);

		//6、渲染指令
		//glClearColor来设置清空屏幕所用的颜色
		glClearColor(0.2f,0.3f,0.4f,1.0f);//设置状态函数
		//glClear函数来清空屏幕的颜色缓冲，它接受一个缓冲位(Buffer Bit)来指定要清空的缓冲，
		//可能的缓冲位有GL_COLOR_BUFFER_BIT，GL_DEPTH_BUFFER_BIT和GL_STENCIL_BUFFER_BIT
		glClear(GL_COLOR_BUFFER_BIT);//状态使用函数 


		// 检查并调用事件，交换缓冲
		glfwSwapBuffers(window);//glfwSwapBuffers函数会交换颜色缓冲（它是一个储存着GLFW窗口每一个像素颜色值的大缓冲），它在这一迭代中被用来绘制，并且将会作为输出显示在屏幕上。
		glfwPollEvents();//glfwPollEvents函数检查有没有触发什么事件（比如键盘输入、鼠标移动等）、更新窗口状态，并调用对应的回调函数（可以通过回调方法手动设置）。
	}


	//当渲染循环结束后我们需要正确释放/删除之前的分配的所有资源。
	//我们可以在main函数的最后调用glfwTerminate函数来完成。
	glfwTerminate();
	return 0;
}

//定义这个回调函数
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

/*
		我们同样也希望能够在GLFW中实现一些输入控制，这可以通过使用GLFW的几个输入函数来完成。
		我们将会使用GLFW的glfwGetKey函数，它需要一个窗口以及一个按键作为输入。
		这个函数将会返回这个按键是否正在被按下。
		我们将创建一个processInput函数来让所有的输入代码保持整洁。
*/
void processInput(GLFWwindow *window)
{
	/*
	 检查用户是否按下了返回键(Esc)
	（如果没有按下，glfwGetKey将会返回GLFW_RELEASE。如果用户的确按下了返回键，
	我们将通过glfwSetwindowShouldClose使用把WindowShouldClose属性设置为 true的方法关闭GLFW。
	*/
	if (glfwGetKey(window,GLFW_KEY_ESCAPE)==GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window,true);
	}
}